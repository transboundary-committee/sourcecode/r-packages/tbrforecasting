

#' @title (Write scripts) Build, save, and open an R script.
#'
#' @param x A character vector. The values can be obtained from
#'   \code{demo(package = "PSCtools")}.
#' @param package A character vector. Default is the current package.
#'
#' @description This writes (to the working directory) and opens an R script
#'   specific to the task identified in argument x. This is a template for the
#'   user to work with.
#'
#' @return Opens an R script that includes the template of functions.
#' @export
#'
#' @examples
#' \dontrun{
#' #to find what scripts available run:
#' demo(package = "PSCtools")
#'
#' writeScript("demo")
#'
#' }
writeScript <- function(x, package="PSCtools"){
  filenames <- paste0(x,".R")
  demo.filenames <- system.file("demo", filenames, package = package)
  file.copy(demo.filenames, to= ".")
  file.edit(basename(demo.filenames))
}#END writeScript


