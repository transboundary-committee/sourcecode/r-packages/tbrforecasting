str(data.matrates)

col.keep <- c("brood.year", "agedesignation.Eu", "prop")
data.matrates.wide <- reshape(data.matrates[data.matrates$prop<1,col.keep], direction = 'wide', idvar="brood.year", timevar =  "agedesignation.Eu")
data.matrates.wide$prop.1.2.1.3 <- data.matrates.wide$prop.1.2+data.matrates.wide$prop.1.3

upper.panel<-function(x, y){
	points(x,y, pch=19)
	r <- round(cor(x, y,use = "complete.obs"), digits=2)
	txt <- paste0("R = ", r)
	usr <- par("usr"); on.exit(par(usr))
	par(usr = c(0, 1, 0, 1))
	text(0.5, 0.9, txt)
}
pairs(data.matrates.wide[complete.cases(data.matrates.wide),c(1,5,4,6,2,3)], lower.panel = upper.panel, 
			upper.panel = upper.panel)
