# TBRforecasting

An R package of tools that aid with forecasting work

To install in R:
    
```
install.packages("remotes") 
remotes::install_git("https://gitlab.com/transboundary-committee/sourcecode/r-packages/TBRforecasting")
```
<!--
You need to enter your GitLab username and password if you get the following error: Error in 'git2r_remote_ls'. 
Run this code instead, adding in your username and password:
```
install.packages("remotes") 
remotes::install_git("https://gitlab.com/chinook-technical-committee/programs/r-packages/eratools", credentials=git2r::cred_user_pass("username","password")
```
-->

Load `TBRforecasting` into memory:
```
require(TBRforecasting)
```

The function `writeScript` will make it easier to start things. The help file has an example:
```
?TBRforecasting::writeScript()
```

In the example we can see how to seek what demo scripts are available:
```
demo(package = "TBRforecasting")
```

To save and open a specific demo script (this one named "demo"):
```
writeScript("demo")

```
